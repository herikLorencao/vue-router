import Vue from 'vue'

export default new Vue({
    // Por ser um instância estática não precisa do return no data
    data: {
        autenticado: false
    },
    created() {
        // Escuta o $emit('autenticar', valor_autenticado)
        this.$on('autenticar', autenticado => {
            this.autenticado = autenticado
        })
    }
})