/* eslint-disable */
import Vue from "vue";
import VueRouter from "vue-router";

import Login from "@/views/login/Login.vue";
import Erro404 from "@/views/Erro404.vue";
import Erro404Contatos from "@/views/contatos/Erro404Contatos.vue";

import EventBus from "./event-bus";

// Carregamento do componente sobre demanda (Lazy Loading)
// import() recurso do webpack
// webpackChunkName (Recurso do webpack para agrupamento de um bundle)
const Contatos = () => import(/* webpackChunkName: "contatos" */ "./views/contatos/Contatos.vue");
const ContatoDetalhes = () => import(/* webpackChunkName: "contatos" */ "./views/contatos/ContatoDetalhes.vue");
const ContatoEditar = () => import(/* webpackChunkName: "contatos" */ "./views/contatos/ContatoEditar.vue");
const ContatoHome = () => import(/* webpackChunkName: "contatos" */ "./views/contatos/ContatoHome.vue");
const Home = () => import('./views/Home.vue');

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  linkExactActiveClass: "active",
  scrollBehavior(to, from, savedPosition) {
    // Não precisa de promise é só para simular comportamento assíncrono
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (savedPosition) {
          return resolve(savedPosition);
        }
        if (to.hash) {
          return resolve({
            selector: to.hash,
            offset: { x: 0, y: 0 }
          });
        }
        resolve({ x: 0, y: 0 });
      }, 3000);
    });
  },
  routes: [
    {
      path: "/contatos",
      component: Contatos,
      alias: ["/meus-contatos", "/lista-de-contatos"],
      props: route => {
        const busca = route.query.busca;
        return busca ? { busca } : {};
      },
      children: [
        {
          path: ":id",
          component: ContatoDetalhes,
          name: "contato",
          props: route => ({
            id: +route.params.id
          })
        } /** /contatos/1 */,
        {
          // path: ":id/editar",
          // path: ':id(\\d+)/editar/:opcional?',
          // path: ':id(\\d+)/editar/:zeroOuMais*',
          // path: ':id(\\d+)/editar/:umOuMais+',
          path: ":id(\\d+)/editar",
          alias: ":id/alterar",
          meta: {
            requerAutenticacao: true
          },
          beforeEnter(to, from, next) {
            console.log("beforeEnter");
            next(); // continuar
            // next(true)
            // next(false) // bloquear
            // next('/contatos') // redirecionar
            // next({ name: 'contatos' })
            // next(new Error(`Permissões insuficientes para acessar o recurso "${to.fullPath}"`))
          },
          components: {
            default: ContatoEditar,
            "contato-detalhes": ContatoDetalhes
          },
          props: {
            default: true,
            "contato-detalhes": true
          }
        } /** /contatos/1/editar */,
        { path: "/home", component: ContatoHome, name: "contatos" },
        { path: "*", component: Erro404Contatos }
      ]
    },
    {
      path: "/",
      component: Home,
      redirect: to => {
        return { name: "contatos" };
      }
    },
    {
      path: "/login",
      component: Login
    },
    {
      path: "*",
      component: Erro404
    }
  ]
});

router.beforeEach((to, from, next) => {
  console.log("beforeEach");
  console.log("Requer autenticação?", to.meta.requerAutenticacao);
  const estaAutenticado = EventBus.autenticado;
  // console.log(to.matched) // urls que contemplam esse rota
  // some() - percorre o array até achar true
  if (to.matched.some(rota => rota.meta.requerAutenticacao)) {
    if (!estaAutenticado) {
      next({
        path: "/login",
        query: {
          redirecionar: to.fullPath
        }
      });
      return;
    }
  }
  next();
});

router.beforeResolve((to, from, next) => {
  console.log("beforeResolve");
  next();
});

router.afterEach((to, from) => {
  console.log("afterEach");
});

// Captura erro lançado nos hooks
router.onError(erro => {
  console.log(erro);
});

export default router;
